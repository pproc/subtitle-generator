/**
 * Created by Piotr Proc on 2018-05-31.
 */

var subtitlesChanger = {
    changeSubtitles: function(subtitles, transcript) {
        var me = this,
            i, line, subtitleWords, changedWords, simplifiedChangedWords,
            transcriptWords = transcript.split(/\s+/g);

        for (i = 0; i < subtitles.length; i++) {
            line = subtitles[i].text;
            subtitleWords = line.split(/\s/g);

            changedWords = me.replaceWords(subtitleWords, transcriptWords);
            simplifiedChangedWords = arrayUtils.simplifyArray(changedWords);
            subtitles[i].text = simplifiedChangedWords.join(" ");
        }

        return subtitles;
    },

    replaceWords: function(subtitleWords, transcriptWords) {
        var me = this,
            resultElement,
            skippedWords = [];

        return subtitleWords.map(function(subtitleWord) {
            var index = 0,
                wordFromTranscript = transcriptWords[index];


            if (stringUtils.compareWords(wordFromTranscript, subtitleWord)) {
                resultElement = arrayUtils.returnLastElement(transcriptWords, skippedWords);
                skippedWords = [];
                return resultElement;
            }

            var searchIndex = me.getIndexOfWordInFurtherTranscript(transcriptWords, subtitleWord);

            if(searchIndex){
                resultElement = arrayUtils.returnSubArray(transcriptWords, searchIndex, skippedWords);
                skippedWords = [];
                return resultElement;
            }

            skippedWords.push(wordFromTranscript);

            return null;
        })
    },

    getIndexOfWordInFurtherTranscript: function(transcriptSplit, word) {
        var nearIndexesLimit = 5;

        for (var searchIndex = 1; searchIndex < nearIndexesLimit; searchIndex++) {
            if(searchIndex >= transcriptSplit.length){
                return null;
            }

            var nextWordFromTranscript = transcriptSplit[searchIndex];

            if (stringUtils.compareWords(nextWordFromTranscript, word)) {
                return searchIndex;
            }
        }

        return null;
    }
};