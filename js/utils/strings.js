/**
 * Created by Piotr Proc on 2018-06-03.
 */

var stringUtils = {

    compareWords: function(baseWord, comparedWord) {
        if(!baseWord){
            return !comparedWord;
        }

        if(!comparedWord){
            return false;
        }

        return this.formatWord(baseWord) === this.formatWord(comparedWord);
    },

    formatWord: function(word){
        return this.wordWithoutPunctualMarks(word).toUpperCase();
    },

    wordWithoutPunctualMarks: function(word) {
        var punctuationMarks = [".", ",", "?", "!",";","“","”","‘","’"],
            punctuationMarksText = punctuationMarks.join(""),
            regexText = this.groupOfElements(punctuationMarksText),
            regex = new RegExp(regexText, "g");

        return word.replace(regex, "");
    },

    groupOfElements: function(text) {
        return "[" + text + "]";
    }
};