/**
 * Created by Piotr Proc on 2018-06-03.
 */

var arrayUtils = {
    simplifyArray: function(array) {
        var index, element;

        for (index = 0; index < array.length; index++) {
            element = array[index];

            if (Array.isArray(element)) {
                this.replaceArrayElementWithItsContent(array, element, index)
            }
        }

        return array.filter(function(element) {
            return element !== null;
        })
    },

    replaceArrayElementWithItsContent: function(array, elements, index) {
        this.removeElement(array, index);

        return this.insertElements(array, elements, index);
    },

    removeElement: function(array, index) {
        array.splice(index, 1);
    },

    insertElements: function(array, elements, index) {
        var indexOffset = 0;

        elements.forEach(function(element) {
            array.splice(index + indexOffset, 0, element);
            indexOffset++;
        });

        return array;
    },

    returnLastElement: function(transcriptSplit, skippedWords) {
        var resultElement = transcriptSplit[0].trim(),
            uniqueSkippedWords;

        transcriptSplit.shift();

        uniqueSkippedWords = skippedWords.filter(function(value) {
            return resultElement.indexOf(value) === -1;
        });

        return uniqueSkippedWords.concat(resultElement);
    },

    returnSubArray: function(originalArray, endIndex, skippedWords) {
        var resultElements = originalArray.splice(0, endIndex + 1),
            uniqueSkippedWords;

        uniqueSkippedWords = skippedWords.filter(function(value) {
            return resultElements.indexOf(value) === -1;
        });

        return uniqueSkippedWords.concat(resultElements);
    }
};