/**
 * Created by Piotr Proc on 2018-06-07.
 */

var validator = {
    WORD_SEPARATOR_REGEX: /\s+/g,
    NEAR_ELEMENTS_DISTANCE: 5,

    validateSubtitles: function(subtitles, transcript) {
        var me = this,
            transcriptArray = transcript.split(me.WORD_SEPARATOR_REGEX),
            transcriptsAreEqual = true,
            gap = me.NEAR_ELEMENTS_DISTANCE,
            index, lastIndex;

        for (index = 0; index < transcriptArray.length && transcriptsAreEqual; index++) {
            transcriptsAreEqual = me.validateSubtitle(index, subtitles, transcriptArray);
        }

        if (!transcriptsAreEqual) {
            lastIndex = index - 1;

            return {
                index: lastIndex,
                percentage: ((lastIndex / transcriptArray.length) * 100).toFixed(2),
                subtitleWord: me.getSubtitleWordNumber(subtitles, lastIndex),
                subtitleNearWords: me.getSubtitleWordNumbers(subtitles, lastIndex - gap, lastIndex + gap),
                transcriptWord: me.getTranscriptWordNumber(transcriptArray, lastIndex),
                transcriptNearWords: me.getTranscriptWordNumbers(transcriptArray, lastIndex - 5, lastIndex + 5)
            };
        }

        return transcriptsAreEqual;
    },

    validateSubtitle: function(index, subtitles, transcriptArray) {
        var subtitleWord = this.getSubtitleWordNumber(subtitles, index),
            transcriptWord = this.getTranscriptWordNumber(transcriptArray, index);

        return stringUtils.compareWords(subtitleWord, transcriptWord);
    },

    getTranscriptWordNumbers: function(transcriptArray, startIndex, endIndex) {
        return this.getXWordNumbers(transcriptArray, startIndex, endIndex, this.getTranscriptWordNumber.bind(this))
    },

    getTranscriptWordNumber: function(transcriptArray, index) {
        return index < transcriptArray.length ? transcriptArray[index] : undefined;
    },

    getSubtitleWordNumbers: function(subtitles, startIndex, endIndex) {
        return this.getXWordNumbers(subtitles, startIndex, endIndex, this.getSubtitleWordNumber.bind(this))
    },

    getXWordNumbers: function(subtitles, startIndex, endIndex, callback) {
        var i, resultArray = [],
            subtitle;

        for (i = startIndex; i <= endIndex; i++) {
            subtitle = callback(subtitles, i);

            if (subtitle) {
                resultArray.push(subtitle)
            }
        }

        return resultArray;
    },

    getSubtitleWordNumber: function(subtitles, wordIndex) {
        var me = this,
            searchIndex = wordIndex,
            searchWord = null,
            i = 0,
            numberOfWordsInSubtitle,
            subtitleWords, subtitleText,
            combinedText;

        while (i < subtitles.length && !searchWord) {
            subtitleText = subtitles[i].text;
            combinedText = Array.isArray(subtitleText) ? subtitleText.join(" ") : subtitleText;
            subtitleWords = combinedText.split(me.WORD_SEPARATOR_REGEX);
            numberOfWordsInSubtitle = subtitleWords.length;

            if (searchIndex < numberOfWordsInSubtitle) {
                searchWord = subtitleWords[searchIndex];
            }

            searchIndex -= numberOfWordsInSubtitle;
            i++;
        }

        return searchWord;
    },

    printOutMessage: function(validationResult) {
        var alertMessage, consoleMessage;

        alertMessage = "Generated subtitles are correct up to: " + validationResult.percentage + "%";

        if (validationResult !== true) {
            consoleMessage +=
                "\n\nSubtitles start to differ on index " + validationResult.index +
                "\non transcript word: " + validationResult.transcriptWord +
                "\non transcript near words: " + validationResult.transcriptNearWords +
                "\n\non subtitles word: " + validationResult.subtitleWord +
                "\non subtitles near words: " + validationResult.subtitleNearWords;

            console.log(consoleMessage);
        }

        alert(alertMessage);
    }
};