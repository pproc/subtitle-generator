/**
 * Created by Piotr Proc on 2018-05-31.
 */

var submitButton = document.getElementById("submitButton"),
    scriptTextarea = document.getElementById("scriptTextarea"),
    automaticSubtitlesTextarea = document.getElementById("automaticSubtitlesTextarea"),
    outputTextarea = document.getElementById("outputTextarea");

submitButton.addEventListener("click", function(event) {
    event.preventDefault();
    var transcriptText = scriptTextarea.value,
        automaticSubtitlesText = automaticSubtitlesTextarea.value,
        formattedSubtitles, changedSubtitlesAsArray,
        changedSubtitles, formattedSubtitlesArray,
        validationResult;

    formattedSubtitles = converterToArray.prepareSubtitles(automaticSubtitlesText);
    changedSubtitlesAsArray = subtitlesChanger.changeSubtitles(formattedSubtitles, transcriptText);
    formattedSubtitlesArray = formatter.formatSubtitles(changedSubtitlesAsArray);
    changedSubtitles = converterToSubtitles.convertToSubtitles(formattedSubtitlesArray);
    validationResult = validator.validateSubtitles(changedSubtitlesAsArray, transcriptText);
    validator.printOutMessage(validationResult);

    outputTextarea.value = changedSubtitles;
    fileDownloader.download(changedSubtitles, "subtitles.srt");
});
