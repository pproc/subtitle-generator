var formatter = {
    MAX_TEXT_LENGTH_PER_LINE: 42,
    SPACE_CHARACTER: " ",

    formatSubtitles: function(textArray) {
        return textArray.map(this.splitSubtitle.bind(this));
    },

    splitSubtitle: function(subtitle) {
        if (this.isSubtitleTextTooLong(subtitle)) {
            return this.splitIntoTwoParts(subtitle);
        }

        return subtitle;
    },

    isSubtitleTextTooLong: function(subtitle) {
        var LIMIT = this.MAX_TEXT_LENGTH_PER_LINE;

        return subtitle.text.length > LIMIT
            && subtitle.text.length < 2 * LIMIT;
    },

    splitIntoTwoParts: function(subtitle) {
        var me = this,
            text = subtitle.text,
            spacePositions = me.getNumberOfOccurrenceInText(text, me.SPACE_CHARACTER),
            hasTextBeenSplit = false,
            spacePosition, index,
            firstPart, secondPart;

        for (index = spacePositions.length - 1; index >= 0 && !hasTextBeenSplit; index--) {
            spacePosition = spacePositions[index];

            firstPart = text.slice(0, spacePosition);
            secondPart = text.slice(spacePosition + 1, text.length);
            hasTextBeenSplit = me.testSplitParts(firstPart, secondPart);
        }

        subtitle.text = [firstPart, secondPart];
        return subtitle;
    },

    getNumberOfOccurrenceInText: function(text, seekingPhrase) {
        var regex = new RegExp(seekingPhrase, "g"),
            match = regex.exec(text),
            positions = [];

        while (match) {
            positions.push(match.index);
            match = regex.exec(text);
        }

        return positions;
    },

    testSplitParts: function(text1, text2) {
        var LIMIT = this.MAX_TEXT_LENGTH_PER_LINE;

        return text1.length <= LIMIT
            && text2.length <= LIMIT;
    }
};