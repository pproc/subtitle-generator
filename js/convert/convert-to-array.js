/**
 * Created by Piotr Proc on 2018-05-31.
 */

var converterToArray = {
    BREAK_SIGN: "\n",

    prepareSubtitles: function(text){
        var subtitlesArray = this.splitSubtitlesIntoPhrases(text),
            clearSubtitlesArray = this.removeHtmlTags(subtitlesArray);

        return this.formatSubtitles(clearSubtitlesArray);
    },

    splitSubtitlesIntoPhrases: function(text){
        var splitRegex = /^\d+\n/gm;
        // Find the lines containing only digitals

        return text.split(splitRegex).slice(1);
    },

    removeHtmlTags: function(array){
        var htmlRegex = /<.*?>/g;

        return array.map(function(line){
            return line.replace(htmlRegex, "");
        });
    },

    formatSubtitles: function(array){
        var me = this;

        return array.map(function(line){
            var splitedLine = line.trim().split(me.BREAK_SIGN);

            if(splitedLine.length !== 2){
                return {};
            }

            return {
                timeCode: splitedLine[0],
                text: splitedLine[1]
            }
        });
    }
};
