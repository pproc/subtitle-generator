/**
 * Created by Piotr Proc on 2018-06-07.
 */

var converterToSubtitles = {
    GAP_WITHIN_SUBTITLE: "\n",
    GAP_BETWEEN_SUBTITLES: "\n\n",

    convertToSubtitles: function(array) {
        var me = this,
            preparedArray;

        preparedArray = array.map(function(subtitle, index) {
            var indexLine = (index + 1);

            return me.createLine(subtitle, indexLine);
        });

        return preparedArray.join(me.GAP_BETWEEN_SUBTITLES);
    },

    createLine: function(subtitle, index) {
        var me = this,
            content;

        if (Array.isArray(subtitle.text)) {
            content = subtitle.text[0] + me.GAP_WITHIN_SUBTITLE + subtitle.text[1];
        }
        else {
            content = subtitle.text;
        }

        return index + me.GAP_WITHIN_SUBTITLE + subtitle.timeCode + me.GAP_WITHIN_SUBTITLE + content;
    }
};